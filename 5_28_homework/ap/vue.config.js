const bodyparser = require('body-parser')
const Mock = require('mockjs')

const data = Mock.mock({
    "list|15":[{
        name:"@cname",
        img:"@image(100x100,@color)",
        'pirect|10-30':20
    }]
})


module.exports = {
    devServer:{
        before(app){
            app.use(bodyparser.json())
            app.get('/list', (req, res) => {
                res.send(data.list) 
            })
            app.post('/login', (req, res) => {
                const {username,password} = req.body
                if(username == 'zzb' && password == '0201'){
                    res.send({
                        code:1,msg:'登陆成功'
                    })
                }else{
                    res.send({
                        code:2,msg:'登陆失败'
                    }) 
                }
            })
        }
    }
}