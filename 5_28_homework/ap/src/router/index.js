import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    name:'Index',
    redirect: '/home',  // 重定向:重新指向其它path,会改变网址
  },
  {
    path: '/home',
    name: 'Home',
    component: ()=>import('../views/Home.vue'),
    redirect: '/home/page',  // 重定向:重新指向其它path,会改变网址
    children:[
      {
        path: 'page',
        name: 'Page',
        component: () => import( '../views/home/Page.vue')
      },
      {
        path: 'list',
        name: 'List',
        component: () => import( '../views/home/List.vue')
      },
      {
        path: 'car',
        name: 'Car',
        component: () => import( '../views/home/Car.vue')
      },
      {
        path: 'my',
        name: 'My',
        component: () => import( '../views/home/My.vue')
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    component: () => import( '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
