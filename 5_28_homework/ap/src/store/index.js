import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    list:[]
  },
  mutations: {
  },
  actions: {
    getList(vuex){
      if(!vuex.state.list.length){
        axios.get('/list').then((res) => {
           vuex.state.list=res.data
        })
      }
    }
  },
  modules: {
  }
})
