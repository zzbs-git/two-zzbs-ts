const fs = require('fs')
const Mock = require('mockjs')
const data = Mock.mock({
    'list|10': [{
        img: '@image(100x100, @color)',
        title: '@ctitle(3,8)',
        'price|1-100': 0,
        cont: 0
    }]
})

fs.writeFileSync('./mock.json',JSON.stringify(data.list))