const list = require('./src/mock/mock.json')

module.exports = {
    devServer: {
        before(app) {
            app.get('/list',(req, res)=>{
                res.send(list)
            })
        }
    }
}