// ts定义一个元素类型为string类型的变量
let string:string='zzb'
// ts定义一个元素类型为number类型的变量
let number:number=1
// 2种方式定义一个元素类型可以为字符串或者数字或者布尔值类型的数组
let any:[string,boolean,number]=['123',false,1]
let any2:Array<number>|Array<string>|Array<boolean>=[1,2,3]
// Ts定义一个元素类型第一个是字符串，第二个是数值的元组
let Tsarr:[string,number]=['111',111]

//length-1
let unLength=(a:any[]):any[]=>{
    let num:number=(a as []).length-1;
    a.length=num;
    return a
}

let unLength2=(a:number[]):number[]=>{
    let arr:number[]=[];
   for (let i = 0; i < a.length; i++) {
       if(i!=a.length-1){
           arr.push(a[i])
       }
   }
   return a
}
let unLengthARR=[1,2,3]
console.log(unLength(unLengthARR));
console.log(unLength2(unLengthARR));


//模拟push方法
let Npush=(a:number[],b:number[]):number[]=>{
    let bnum:number=(b as number[]).length;
    if(Array.isArray(b)){
       for (let i = 0; i < bnum; i++) {
        let anum:number=(a as number[]).length;
           a[anum]=b[i]
       }
    }else{
        let anum:number=(a as number[]).length;
        a[anum]=b[0]
       }
    return a
}
let Npushsarr=[1,2,3,4,5]
console.log(Npush(Npushsarr,[1,2]));
console.log(Npush(Npushsarr,[5]));



//数组二维转一维
// let arr:number[]=[1,2,3,[4,5]]
let TzY=(a:any[]):number[]=>{
    let Yarr:number[]=[]
    // let Yarr2:number[]=[]
   for (let i = 0; i < a.length-1; i++) {
        if(a[i].length!=1){
            console.log(11);
           for (let j = 0; j < a[i].length-1; j++) {
                Yarr.push(a[i][j])
           }
        }else{
            Yarr.push(a[i])
        }
   }
   return Yarr
}
console.log(TzY([1,2,3,[4,5]]));
