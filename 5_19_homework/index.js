// ts定义一个元素类型为string类型的变量
var string = 'zzb';
// ts定义一个元素类型为number类型的变量
var number = 1;
// 2种方式定义一个元素类型可以为字符串或者数字或者布尔值类型的数组
var any = ['123', false, 1];
var any2 = [1, 2, 3];
// Ts定义一个元素类型第一个是字符串，第二个是数值的元组
var Tsarr = ['111', 111];
//length-1
var unLength = function (a) {
    var num = a.length - 1;
    a.length = num;
    return a;
};
var unLength2 = function (a) {
    var arr = [];
    for (var i = 0; i < a.length; i++) {
        if (i != a.length - 1) {
            arr.push(a[i]);
        }
    }
    return a;
};
var unLengthARR = [1, 2, 3];
console.log(unLength(unLengthARR));
console.log(unLength2(unLengthARR));
//模拟push方法
var Npush = function (a, b) {
    var bnum = b.length;
    if (Array.isArray(b)) {
        for (var i = 0; i < bnum; i++) {
            var anum = a.length;
            a[anum] = b[i];
        }
    }
    else {
        var anum = a.length;
        a[anum] = b[0];
    }
    return a;
};
var Npushsarr = [1, 2, 3, 4, 5];
console.log(Npush(Npushsarr, [1, 2]));
console.log(Npush(Npushsarr, [5]));
//数组二维转一维
// let arr:number[]=[1,2,3,[4,5]]
var TzY = function (a) {
    var Yarr = [];
    // let Yarr2:number[]=[]
    for (var i = 0; i < a.length - 1; i++) {
        if (a[i].length != 1) {
            console.log(11);
            for (var j = 0; j < a[i].length - 1; j++) {
                Yarr.push(a[i][j]);
            }
        }
        else {
            Yarr.push(a[i]);
        }
    }
    return Yarr;
};
console.log(TzY([1, 2, 3, [4, 5]]));
