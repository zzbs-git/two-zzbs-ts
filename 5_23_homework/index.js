"use strict";
// 声明一个类接口，规定定义的类满足以下要求（name属性必传、age属性可选、sex属性只读，同时还拥有eat方法）
class Moder {
    constructor(name, age, sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
    ;
    eat() {
        return this.name;
    }
}
// 编写函数uniq(str)，返回去掉str重复项的字符串
let uniq = (a) => {
    let str = '';
    let s = a.split('');
    let n = s.length;
    for (let i = 0; i < n; i++) {
        if (str.indexOf(s[i]) == -1) {
            str += s[i];
        }
    }
    return str;
};
console.log(uniq('aabbcc'));
// 封装一个泛型类GetMax;实现找出数组中的最大值，以及最大值下标
let GetMax = (a) => {
    let n = 0;
    let num = a.length;
    let max = Math.max.apply(Math, a);
    for (let i = 0; i < num; i++) {
        if (a[i] == max) {
            n = i;
        }
    }
    return n;
};
console.log(GetMax([5, 2, 3, 6, 10]));
// •	封装一个findLongestWord(str)函数，str是个英文句子，实现如下功能：找出str句子中最长的单词，并计算它的长度。例如。"The quick brown fox jumped over the lazy dog." findLongestWord("The quick brown fox jumped over the lazy dog."); //6 findLongestWord("I believe I can do it.");//7
let findLongestWord = (a) => {
    let str = a.split(" ");
    let maxlength = str[0].length;
    for (let i = 0; i < str.length; i++) {
        if (str[i].length > maxlength) {
            maxlength = str[i].length;
        }
    }
    return maxlength;
};
console.log(findLongestWord('zzbzbzbzbzb  is ssss hhh'));
