const Mock = require("mockjs");
const data = Mock.mock({
    "list|20": [{
        'url': '@image(100x100, @color)',
        "title": '@ctitle',
        'price|10-800': 0,
        num: 0,
        isCollect: false,
        id: '@id',
        checked: false,
    }]
})
module.exports = {
    devServer: {
        before(app) {
            app.get("/list", (req, res) => {
                res.send(data.list)
            })

        }
    }
}