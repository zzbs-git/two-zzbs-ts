import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        arr: [],
        detobj: [],
    },
    mutations: {
        setarr(state, payload) {
            state.arr = payload;
        },
        // 筛选
        strarr(state, payload) {
            state.arr.filter((item, index) => {
                if (item.id == payload) {
                    state.detobj = item;
                }
            })

        },
        // num增加
        appnum(state, payload) {
            state.arr = state.arr.map((item, index) => {
                if (item.id == payload) {
                    item.num++;
                }
                return item
            })


        },
        // 减
        deljj(state, payload) {
            state.arr = state.arr.map((item, index) => {
                if (item.id == payload) {
                    item.num--;
                }
                return item
            })


        },
        // 选中未选中
        changex(state, payload) {
            state.arr = state.arr.map((item, index) => {
                if (item.id == payload) {
                    item.checked = !item.checked;
                }
                return item
            })

        },
        // 全选
        quanx(state, payload) {
            state.arr.map((item, index) => {
                return item.checked = payload;
            })
        }


    },
    actions: {
        getlist({ commit, state }, payload) {
            if (state.arr.length <= 0) {
                axios.get("/list").then(res => {
                    commit("setarr", res.data)
                })
            }


        }

    },
    modules: {}
})