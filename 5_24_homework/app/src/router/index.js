import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'all',
        redirect: '/all', // 重定向:重新指向其它path,会改变网址
    },
    {
        path: '/all',
        name: 'all',

        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/all.vue'),
        children: [{
                path: '/all/sy',
                name: 'sy',

                // which is lazy-loaded when the route is visited.
                component: () =>
                    import ( /* webpackChunkName: "about" */ '../views/sy.vue')
            },
            {
                path: '/all/bt',
                name: 'bt',

                // which is lazy-loaded when the route is visited.
                component: () =>
                    import ( /* webpackChunkName: "about" */ '../views/bt.vue')
            }, {
                path: '/all/yc',
                name: 'yc',

                // which is lazy-loaded when the route is visited.
                component: () =>
                    import ( /* webpackChunkName: "about" */ '../views/yc.vue')
            },
        ]
    },

    {
        path: '/my',
        name: 'my',

        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/my.vue')
    },
    {
        path: '/detail/:id',
        name: 'detail',

        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/detail.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router