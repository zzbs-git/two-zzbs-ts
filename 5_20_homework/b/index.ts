//2种方式定义一个数组（数组中的元素可以是任意类型）
let arr:string[] = ['a','s','d']
let arr1:Array<boolean> = [false,true]

// Ts定义一个求和函数;例Add(2,8,9,6,5,8,7),返回结果45
function Add(...a:number[]){
    let num:number=0
   for (let i = 0; i < a.length; i++) {
       num+=a[i]
   }
   return num
}
console.log(Add(1,9));

// Ts定义一个函数返回值为boolean类型的判断函数;例isTrue(nudefined),返回false
function isTrue(a:any){
    if(a){
        return true
    }else{
        return false
    }
}
console.log(isTrue(0));
console.log(isTrue(1));



//补0
let AddZero=(a:number[]):string[]=>{
    let arr=[]
    let num:number=(a as number[]).length 
   for (let i = 0; i < num; i++) {
       if(a[i]<10){
           arr[i]='0'+a[i]
       }else{
           arr[i]=a[i]
       }
   }
    return arr
}
let Zeroarr=[0,1,9,10,11,5]
console.log(AddZero(Zeroarr));



//所有数字*5
let Five=(a:any[]):number[]=>{
    let arr:number[]=[]
    let num:number=(a as number[]).length 
    let n=0
    for (let i = 0; i < num; i++) {
        if(typeof(a[i])=="number"){
            arr[n]=a[i]*5
            n++
        }
    }
    return arr
}
let Fivearr:any[]=[false,10,'123',1]
console.log(Five(Fivearr));

