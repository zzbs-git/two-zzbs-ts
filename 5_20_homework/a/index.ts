// 定义一个数值和字符串的联合类型（10分）
let Nstr:string|number='1'
// 定义一个类型断言（10分）
let Darr:string='asdfghjkl'
let Dnum:number=(Darr as string).length 
// 定义一个 void 实现函数的无返回值（10分）
let fun=():void=>{
    console.log('无返回函数');
}
fun()
// 定义一个 never 的函数（10分）
let fun1=():never=>{
    while(true){
    }
}
fun1()
// 定义函数时， 分别实现参数类型、剩余参数、默认参数（10分）

// 实现一个函数重载（10分）
// 使用 es5 的方式定义一个类和es6中定义类（10分）
// 在 ts 中定义一个类并实现继承（10分）
class Arr{
    name:string;
    age:number;
    constructor(name:string,age:number){
        this.name=name 
        this.age=age
    }

    public say(){  //公共
       console.log('say');
    }

    protected eat(){ //受保护
       console.log('eat');
    }

    private run(){ //私有
       console.log('run');
    }
}
let newArr=new Arr("张智斌",20)

