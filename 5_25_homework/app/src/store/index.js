import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name:'张智斌',
    age:18
  },//存储全局变量
  getters:{
    changeAge(state){
      return state.age+1
    }
  },//state的计算属性,类似于组件的computed
  mutations: {
    editAge(state){
      state.age++
    }
  },//同步处理数据
  actions: {
  },//异步处理数据,和mutations一样都是定义方法去处理数据
  modules: {
  }//在一个大项目里,避免项目臃肿,讲不同业务模块抽离成不同的子仓库进行处理
})
