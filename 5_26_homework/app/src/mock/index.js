import Mock from 'mockjs'

Mock.mock('/api/list',{
    'data|5-8':[{
        "id":'@id',
        "title":'@ctitle(4)',
        "img":'@image(100x100,@color)',
        "price|50-200":50,
    }]
})