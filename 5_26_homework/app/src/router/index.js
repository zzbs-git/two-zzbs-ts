import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home/page', // 重定向:重新指向其它path,会改变网址
},
// 一级路由
{
    path: '/home',
    name: 'home',
    component: () =>
        import ('../views/Home.vue'),
    // 二级路由
    children: [{
        path: '/home/page',
        name: 'page',
        component: () =>
            import ('../views/Page.vue')
    }, {
        path: '/home/classify',
        name: 'classify',
        component: () =>
            import ('../views/Classify.vue')
    }, {
        path: '/home/cart',
        name: 'cart',
        component: () =>
            import ('../views/Cart.vue')
    }, {
        path: '/home/my',
        name: 'my',
        component: () =>
            import ('../views/My.vue')
    }]
}, {
    path: '/detail/:id',
    name: 'detail',
    component: () =>
        import ('../views/Detail.vue')
}
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
