import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    list:[]
  },
  mutations: {
    setList(state,payload){
      state.list=payload.data
    }
  },
  actions: {
    getList(context){
      axios.get('/api/list').then(res=>{
        // console.log(res.data);
        // this.list=res.data.data
        context.commit('setList',res.data)
    })
    }
  },
  modules: {
  }
})
